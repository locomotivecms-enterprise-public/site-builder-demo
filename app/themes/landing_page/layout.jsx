import React from 'react';
import Style from 'style-it';
import Color from 'color';

export default class Layout extends React.Component {

  constructor(props) {
    super(props);
    this.state = { modalIsOpen: false};
  }

  componentDidMount() {
    this.modalListener = window.addEventListener('modal', function (e) {
      this.setState({ modalIsOpen: e.detail.open });
    }.bind(this), false);
  }

  componentWillUnmount() {
    window.removeEventListener(this.modalListener);
  }

  renderFonts() {
    const { brandFont } = this.props.site.style;

    var fonts = [
      'Montserrat:400,700',
      'Droid+Serif:400,700,400italic,700italic',
      'Roboto+Slab:400,100,300,700'
    ]

    if (this.props.options.preview)
      fonts = fonts.concat([
        'Raleway',
        'Open+Sans',
        'Lato',
        'Old+Standard+TT',
        'Abril+Fatface',
        'PT+Serif',
        'Ubuntu',
        'Vollkorn',
        'PT Mono',
        'Gravitas+One',
        'Roboto+Condensed',
        'Fira+Sans',
        'Lora',
        'Signika',
        'Cabin',
        'Oswald',
        'Arimo',
        'Arvo'
      ]);
    else
      fonts.push(brandFont);

    return fonts.map(function(font, index) {
      const url = `https://fonts.googleapis.com/css?family=${font}`;
      return (
        <link key={index} href={url} rel="stylesheet" type="text/css" />
      );
    });
  }

  renderStyle() {
    const { primaryColor} = this.props.site.style;
    const primaryDarken = Color(primaryColor).darken(0.15);
    const primaryMoreDarken = Color(primaryColor).darken(0.25);
    const primaryFade = Color(primaryColor).fade(0.2);

    return (
      <Style>
        {`
          body {
            -webkit-tap-highlight-color: ${primaryColor};
          }
          a {
            color: ${primaryColor};
          }
          a:hover {
            color: ${primaryDarken};
          }
          .text-primary {
            color: ${primaryColor} !important;
          }
          .btn-primary {
            background-color: ${primaryColor};
            border-color: ${primaryColor};
          }
          .btn-primary:hover {
            background-color: ${primaryDarken} !important;
            border-color: ${primaryDarken} !important;
          }
          .btn-primary:focus, .btn-primary:active {
            box-shadow: 0 0 0 0.2rem ${primaryMoreDarken} !important;
          }
          ::-moz-selection {
            background: ${primaryColor};
          }
          ::selection {
            background: ${primaryColor};
          }
          .navbar-toggler {
            background-color: ${primaryColor};
          }
          .navbar-brand {
            color: ${primaryColor} !important;
          }
          .navbar-brand:hover {
            color: ${primaryDarken} !important;;
          }
          .nav-link:hover {
            color: ${primaryColor} !important;
          }
          .carousel-caption-btn {
            background-color: ${primaryColor} !important;
          }
          .form-control:focus {
            border-color: ${primaryColor};
          }
          ul.social-buttons li a:hover {
            background-color: ${primaryColor};
          }
          #portfolio .portfolio-item .portfolio-link .portfolio-hover {
            background: ${primaryFade};
          }
          .timeline-image {
            background-color: ${primaryColor};
          }
          .fa-circle {
           color: ${primaryColor};
          }
       `}
      </Style>
    )
  }

  render() {
    return(
      <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
        <head>
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

          <title>{this.props.site.name}</title>

          <link rel="stylesheet" href={this.props.assetPath('/themes/landing_page/stylesheets/font-awesome.min.css')} />
          <link rel="stylesheet" href={this.props.assetPath('/themes/landing_page/frontend.css')} />
          <script type="text/javascript" src={this.props.assetPath('/themes/landing_page/frontend.js')} />
          <script dangerouslySetInnerHTML={{__html: `
            require('themes/landing_page/frontend/javascripts/main');
          `}} />
          {this.renderStyle()}
          {this.renderFonts()}
        </head>
        <body className={this.state.modalIsOpen ? "modal-open": ""}>
          {this.props.children}
          <div id="modal-root"></div>
        </body>
      </html>
    )
  }

}
