import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderText from './common/text';

export default class Header extends React.Component {

  render() {
    const content = this.props.block.content;
    var backgroundImage = this.props.assetPath("/themes/landing_page/images/wallpaper-header.jpg");

    if(!content.defaultBackground)
      backgroundImage = content.backgroundImage;

    return(
      <SiteBuilderBlock tag="header" className="masthead" block={this.props.block} style={{ backgroundImage: `url(${backgroundImage})` }}>
        <div className="overlay" style={{ backgroundColor: `rgba(0, 0, 0, ${content.overlay ? 0.3 : 0})` }}>
          <div className="container">
            <div className="intro-text">
              <SiteBuilderText tag="p" className="intro-lead-in" value={content.intro_lead} />
              <SiteBuilderText tag="p" className="intro-heading text-uppercase" value={content.intro_heading} />
              <a
              className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
              href={this.props.urlTo(content.buttonTarget_type, content.buttonTarget)}>{content.button_label}
              </a>
            </div>
          </div>
        </div>
      </SiteBuilderBlock>
    );
  }

}
