import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';
import Modal from './misc/modal';

export default class Portfolio extends React.Component {

  constructor(props) {
    super(props);
    this.state = { modalIsOpen: false, item: null };
  }

  openModal(portfolioItem) {
    this.setState({ modalIsOpen: true, item: portfolioItem }, this.fireModalEvent.bind(this));
  }

  closeModal() {
    this.setState({ modalIsOpen: false }, this.fireModalEvent.bind(this));
  }

  fireModalEvent() {
    var event = new CustomEvent('modal', { detail: { open: this.state.modalIsOpen } });
    window.dispatchEvent(event);
  }

  renderPortfolio(portfolioItem, key) {
    return (
      <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 portfolio-item" key={key}>
        <a className="portfolio-link" data-toggle="modal" onClick={this.openModal.bind(this, portfolioItem)}>
          <div className="portfolio-hover">
            <div className="portfolio-hover-content">
              <i className="fa fa-plus fa-3x"></i>
            </div>
          </div>
          <div className="img-fluid d-block mx-auto containerImg" style={{ backgroundImage : `url(${portfolioItem.image})` }}></div>
        </a>
        <div className="portfolio-caption">
          <h4>{portfolioItem.title}</h4>
          <p className="text-muted">{portfolioItem.subtitle}</p>
        </div>
      </div>
    )
  }

  renderModal() {
    const { item } = this.state;

    return (
      <Modal>
        <div className="portfolio-modal modal fade show" tabIndex="-1" role="dialog" aria-hidden="true" style={{ display: 'block' }}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="close-modal" data-dismiss="modal" onClick={this.closeModal.bind(this)}>
                <div className="lr">
                  <div className="rl"></div>
                </div>
              </div>
              <div className="container">
                <div className="row">
                  <div className="col-lg-8 mx-auto">
                    <div className="modal-body">
                      <h2 className="text-uppercase">{item.title}</h2>
                      <p className="item-intro text-muted">{item.subtitle}</p>
                      <img className="img-fluid d-block mx-auto" src={item.image} />
                      <p>{item.description}</p>
                      <ul className="list-inline">
                        <li>Date: {item.date}</li>
                        <li>Client: {item.client}</li>
                        <li>Category: {item.category}</li>
                      </ul>
                      <button className="btn btn-primary" data-dismiss="modal" type="button" onClick={this.closeModal.bind(this)}>
                        <i className="fa fa-times"></i>
                        Close Project</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal-backdrop fade show"></div>
      </Modal>
    )
  }

  render() {
    const content = this.props.block.content;

    return (
      <SiteBuilderBlock tag="section" className="bg-light" id="portfolio" block={this.props.block}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <SiteBuilderText tag="h2" className="section-heading text-uppercase" value={content.title} />
              <SiteBuilderText tag="h3" className="section-subheading text-muted" value={content.subtitle} />
            </div>
          </div>
          <SiteBuilderList className="row" list={content.portfolioList}>
            {this.renderPortfolio.bind(this)}
          </SiteBuilderList>
        </div>
        {this.state.modalIsOpen && this.renderModal()}
      </SiteBuilderBlock>
    );
  }

}
