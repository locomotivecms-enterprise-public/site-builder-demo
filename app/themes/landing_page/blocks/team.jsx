import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Team extends React.Component {

  renderMember(member, key) {
    const { memberList, iconColor } = this.props.block.content;

    return (
      <div className="col-lg-4 col-md-4 col-sm-12" key={key}>
        <div className="team-member">
          <img className="mx-auto rounded-circle" src={member.profilImage} />
          <h4>{member.memberName}</h4>
          <p className="text-muted">{member.jobLabel}</p>
          <ul className="list-inline social-buttons">
            <li className="list-inline-item">
              <a href={member.twitter}>
                <i className="fa fa-twitter"></i>
              </a>
            </li>
            <li className="list-inline-item">
              <a href={member.facebook}>
                <i className="fa fa-facebook"></i>
              </a>
            </li>
            <li className="list-inline-item">
              <a href={member.linkedin}>
                <i className="fa fa-linkedin"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    )
  }

  render() {
    const content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="section" className="bg-light" id="team" block={this.props.block}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <SiteBuilderText tag="h2" className="section-heading text-uppercase" value={content.title} />
              <SiteBuilderText tag="h3" className="section-subheading text-muted" value={content.subtitle} />
            </div>
          </div>
          <SiteBuilderList className="row" list={content.memberList}>
            {this.renderMember.bind(this)}
          </SiteBuilderList>
          <div className="row">
            <div className="col-lg-8 mx-auto text-center">
              <SiteBuilderText tag="p" className="large text-muted" value={content.text} />
            </div>
          </div>
        </div>
      </SiteBuilderBlock>
    );
  }
}
