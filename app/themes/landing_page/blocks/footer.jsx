import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Footer extends React.Component {

  renderLink(item, key) {
    const { linkColor } = this.props.block.content;
    
    return <li className="list-inline-item" key={key}>
            <a href={this.props.urlTo(item.target_type, item.target)}>{item.label}</a>
          </li>
  }
  
  renderIcon(icon, key) {
    const { iconList, iconColor } = this.props.block.content;

    return (
      <li className="list-inline-item" key={key}>
          <a href={icon.url}>
            <i className={`fa ${icon.icon}`} style={{ color: "white" }}></i>
          </a>
      </li>
    )
  }
  
  render() {
    const content = this.props.block.content;
    var date = new Date();
    var year = date.getFullYear();
    
    return(
      <SiteBuilderBlock tag="div" block={this.props.block}>
        <footer>
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <span className="copyright">Copyright &copy; {content.website} {year} </span>
              </div>
              <div className="col-md-4"> 
                <ul className="list-inline social-buttons">
                  <SiteBuilderList list={content.iconList}>
                      {this.renderIcon.bind(this)}
                  </SiteBuilderList>
                </ul>
              </div>
              <div className="col-md-4"> 
                <ul className="list-inline quicklinks">
                  <SiteBuilderList list={content.linkList}>
                    {(element, key) => this.renderLink(element, key) }
                  </SiteBuilderList>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </SiteBuilderBlock>
    );
  }

}
