import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import {
  Collapse, Navbar, NavbarToggler,
  NavbarBrand, Nav, NavItem, NavLink,
  NavDropdown, DropdownMenu, DropdownItem, DropdownToggle, ButtonDropdown
} from 'reactstrap';


export default class NavBlock extends React.Component {

  constructor(props) {
    super(props);
    this.toggleMenu = this.toggleMenu.bind(this);
    this.state = { menuIsOpen: false };
  }

  toggleMenu() {
    this.setState({ menuIsOpen: !this.state.menuIsOpen });
  }

  renderTopMenuItem(item, key) {
    return (
      <NavItem key={key}>
        <NavLink href={item.target} className="js-scroll-trigger">
          {item.label}
        </NavLink>
      </NavItem>
    )
  }

  render() {
    const { menu, title } = this.props.block.content;
    const { brandFont } = this.props.site.style;

    return(
      <SiteBuilderBlock tag={Navbar} block={this.props.block} light id="mainNav" expand="lg" className="mainNav navbar-dark fixed-top">
        <div className="container">
          <NavbarBrand href="#" style={{ fontFamily: brandFont }}>{title}</NavbarBrand>
          <NavbarToggler onClick={this.toggleMenu} />
          <Collapse isOpen={this.state.menuIsOpen} navbar>
            <SiteBuilderList tag={Nav} list={menu} className="navbar-nav text-uppercase ml-auto" navbar>
              {(element, key) => this.renderTopMenuItem(element, key) }
            </SiteBuilderList>
          </Collapse>
        </div>
      </SiteBuilderBlock>
    );
  }

}
