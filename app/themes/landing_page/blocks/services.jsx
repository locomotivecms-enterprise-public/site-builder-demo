import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';
import classNames from 'classnames';

export default class Services extends React.Component {

  renderService(service, key) {
    const { serviceList, iconColor } = this.props.block.content;

    var classNameXL = serviceList.length < 4 ? `col-xl-${12 / serviceList.length}` : `col-xl-3`;
    var classNameLG = serviceList.length < 3 ? `col-lg-${12 / serviceList.length}` : `col-lg-4`;

    var allClassNames = classNames('gi-column', classNameXL, classNameLG, 'col-md-12 col-sm-12 col-xs-12');

    return (
      <div key={key} className={allClassNames}>
        <div className="gi-container">
          <div className="gi-icon">
            <span className="fa-stack fa-4x">
              <i className="fa fa-circle fa-stack-2x"></i>
              <i className={`fa fa-stack-1x fa-inverse ${service.icon}`}></i>
            </span>
          </div>
          <SiteBuilderText className="service-heading" tag="h3" value={service.title} />
          <SiteBuilderText className="text-muted" tag="p" value={service.text} />
        </div>
      </div>
    )
  }

  render() {
    const content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="section" id="services" block={this.props.block}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <SiteBuilderText tag="h2" className="section-heading text-uppercase" value={content.heading} />
              <SiteBuilderText tag="h3" className="section-subheading text-muted" value={content.sub_heading} />
            </div>
          </div>
          <SiteBuilderList tag="div" list={content.serviceList} className="row text-center">
            {this.renderService.bind(this)}
          </SiteBuilderList>
        </div>
      </SiteBuilderBlock>
    );
  }

}
