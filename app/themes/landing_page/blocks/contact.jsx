import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Contact extends React.Component {

  render() {
    const content = this.props.block.content;
    var backgroundImage = this.props.assetPath("/themes/landing_page/images/world-bg.png");

    if (!content.defaultBackground)
      backgroundImage = content.backgroundImage;

    return(
      <SiteBuilderBlock tag="section" id="contact" style={{ backgroundImage: `url(${backgroundImage})` }} block={this.props.block}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <SiteBuilderText tag="h2" className="section-heading text-uppercase" value={content.title} />
              <SiteBuilderText tag="h3" className="section-subheading text-muted" value={content.subtitle} />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <form id="contactForm" name="sentMessage">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <input className="form-control" id="name" type="text" placeholder={content.name} required />
                    </div>
                    <div className="form-group">
                      <input className="form-control" id="email" type="email" placeholder={content.email} required />
                    </div>
                    <div className="form-group">
                      <input className="form-control" id="phone" type="tel" placeholder={content.phone} required />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <textarea className="form-control" id="message" placeholder={content.message} required></textarea>
                    </div>
                  </div>
                </div>
                <div className="clearfix"></div>
                <div className="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" className="btn btn-primary btn-xl text-uppercase" type="submit">{content.button_label}</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </SiteBuilderBlock>
    );
  }

}
