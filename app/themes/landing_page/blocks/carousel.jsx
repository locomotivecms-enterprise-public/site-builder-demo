import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import SiteBuilderBlock from './common/block';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

import { Carousel as BsCarousel, CarouselItem as BsCarouselItem, CarouselControl as BsCarouselControl, CarouselIndicators as BsCarouselIndicators, CarouselCaption as BsCarouselCaption } from 'reactstrap';

const CarouselCaption = (props) => {
  const { title, buttonLabel, buttonUrl, showButton } = props;

  return (
    <div className="carousel-caption">
      <div className="carousel-caption-panel">
        <SiteBuilderText tag="h2" value={title} />
        {showButton && (<a className="carousel-caption-btn btn btn-primary btn-xl text-uppercase js-scroll-trigger" href={buttonUrl}>{buttonLabel}</a>)}
      </div>
    </div>
  );
};

// Hack to remove the warning....
BsCarouselItem.propTypes = {
  in: PropTypes.bool, src: PropTypes.string.isRequired, altText: PropTypes.string,
  cssModule: PropTypes.object, children: PropTypes.shape({ type: PropTypes.oneOf([CarouselCaption]) }),
  slide: PropTypes.bool
};

export default class Carousel extends React.Component {

  constructor(props) {
    super(props);
    this.list  = this.buildListFromProps(props);
    this.state = { activeIndex: 0 };
    ['next', 'previous', 'goToIndex', 'onExiting', 'onExited', 'onSlideEditing']
    .forEach((name) => this[name] = this[name].bind(this));
  }

  componentWillUpdate(nextProps, nextState) {
    this.list = this.buildListFromProps(nextProps);
  }

  componentDidMount() {
    document.addEventListener('editing_list_item', this.onSlideEditing, false);
  }

  onSlideEditing(event) {
    var item = event.detail.item;
    if (item.blockId !== this.props.block.id) return null;
    this.goToIndex(item.position);
  }

  buildListFromProps(props) {
    const slides = props.block.content.slides;

    if (slides === undefined || slides.length === 0) return null;

    // the ReactStrap Carousel expects a key attribute in a Slide object.
    // + sort the list
    return slides
      .map((slide) => Object.assign({}, { key: slide.id }, slide))
      .sort((a, b) => (a.position || 0) - (b.position || 0));
  }

  onExiting() { this.animating = true; }
  onExited()  { this.animating = false; }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.list.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous(items) {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.list.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  renderSlides() {
    return this.list.map((slide, index) => {
      var content = slide.content;
      var url     = this.props.urlTo(content.buttonUrl_type, content.buttonUrl);

     return (
        <BsCarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={slide.id}
          src={slide.content.backgroundImage}
        >
         <CarouselCaption
          title={content.title}
          showButton={content.showButton}
          buttonLabel={content.buttonLabel}
          buttonUrl={url} />

        </BsCarouselItem>
      );
    });
  }

  render() {
    const { activeIndex } = this.state;
    var content = this.props.block.content;
    var editing = this.props.contentEditing;

    // don't render the carousel if no slide
    if (this.list === null) return null;

    return(
      <SiteBuilderBlock className={classNames('slider')} block={this.props.block}>
        <div id="carousel" className={content.size}>
          <BsCarousel
            activeIndex={activeIndex}
            next={this.next}
            previous={this.previous}
            pause={'hover'}
            interval={editing ? 0 : content.interval}
            >
              <BsCarouselIndicators items={this.list} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
              {this.renderSlides()}
              <BsCarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
              <BsCarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
          </BsCarousel>
        </div>
      </SiteBuilderBlock>
    );
  }

}
