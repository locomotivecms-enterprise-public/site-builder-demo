import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderText from './common/text';

export default class Text extends React.Component {

  render() {
    const content = this.props.block.content;
    const { backgroundColor, textColor, titleColor } = this.props.block.content;

    return(
      <SiteBuilderBlock tag="div" className="container block-text" block={this.props.block} style={{ backgroundColor: backgroundColor }}>
        <div className="row justify-content-center">
          <SiteBuilderText tag="h2" className="text-uppercase" value={content.title} style={{ color: titleColor }} />
        </div>
        <div className="row">
          <SiteBuilderText tag="p" className="align-items-center" value={content.body} style={{ color: textColor }} />
        </div>
      </SiteBuilderBlock>
    );
  }

}
