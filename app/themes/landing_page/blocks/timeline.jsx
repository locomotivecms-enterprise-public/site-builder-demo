import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Timeline extends React.Component {

  renderSlots(slot, key, index) {
    return (
      <li key={key} className={((index % 2) == 0) ? "" : "timeline-inverted"}>
        <div className="timeline-image" style={{ backgroundImage: `url(${slot.image})` }}></div>
        <div className="timeline-panel">
          <div className="timeline-heading">
            <h4>{slot.date}</h4>
            <h4 className="subheading">{slot.title}</h4>
          </div>
          <div className="timeline-body">
            <p className="text-muted">{slot.text}</p>
          </div>
        </div>
      </li>
    )
  }

  render() {
    const content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="section" id="about" block={this.props.block}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <SiteBuilderText tag="h2" className="section-heading text-uppercase" value={content.title} />
              <SiteBuilderText tag="h3" className="section-subheading text-muted" value={content.subtitle} />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <SiteBuilderList tag="ul" className="timeline" list={content.slots}>
                {this.renderSlots.bind(this)}
              </SiteBuilderList>
            </div>
          </div>
        </div>
      </SiteBuilderBlock>
    );
  }

}
