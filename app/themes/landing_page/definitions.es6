// File generated by the Locomotive SiteBuilder CLI. DO NOT MODIFY IT!

import block_nav from "./blocks/nav";
import block_header from "./blocks/header";
import block_services from "./blocks/services";
import block_portfolio from "./blocks/portfolio";
import block_carousel from "./blocks/carousel";
import block_text from "./blocks/text";
import block_timeline from "./blocks/timeline";
import block_team from "./blocks/team";
import block_contact from "./blocks/contact";
import block_footer from "./blocks/footer";
import layout from "./layout";


export default {
	blocks: {
		nav: block_nav,
		header: block_header,
		services: block_services,
		portfolio: block_portfolio,
		carousel: block_carousel,
		text: block_text,
		timeline: block_timeline,
		team: block_team,
		contact: block_contact,
		footer: block_footer
	},
	layout: layout
}
// done