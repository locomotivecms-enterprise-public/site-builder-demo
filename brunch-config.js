module.exports = {
  files: {
    javascripts: {
      entryPoints: {
        // --- JS THEME FILE BELOW - DO NOT REMOVE THIS LINE
        'app/themes/landing_page/frontend/javascripts/main.es6': 'themes/landing_page/frontend.js',
        'app/themes/landing_page/client.es6': 'themes/landing_page/site_builder.js'
      }
    },
    stylesheets: {
      joinTo: {
        // --- CSS THEME FILE BELOW - DO NOT REMOVE THIS LINE
        'themes/landing_page/frontend.css': /^app\/themes\/landing_page\/frontend\/stylesheets/,


        'site_builder/app.css': /^app\/site_builder\/stylesheets/
      },
      order: {
        before: []
      }
    }
  },

  plugins: {
    babel: {
      babelrc: true,
      pattern: /\.(es6|jsx)$/,
      ignore: [/^(assets|node_modules)/],
      presets: ['es2015', 'react']
    },
    sass: {
      mode: 'native',
      options: {
        includePaths: [
          "node_modules/bootstrap/scss",
        ], // tell sass-brunch where to look for files to @import
        precision: 8 // minimum precision required by bootstrap-sass
      }
    }
  },

  hooks: {
    preCompile: function(cb) {
      var execSync = require('child_process').execSync;
      console.log(execSync('locomotive build').toString());
      cb();
    },
  }

};
